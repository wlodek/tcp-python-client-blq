#!/usr/bin/env python3

import time
import select
import socket
import codecs
import argparse
import ipaddress

from scapy.compat import raw
from scapy.layers import dhcp6
from scapy.layers import dhcp


class TcpClient:
    def __init__(self, addr, port):
        self.addr = addr
        self.port = port

    def set_address(self, a: str):
        """
        Set new address
        :param a: ip address of remote host
        """
        self.addr = a

    def set_port(self, a: int):
        """
        Set new port
        :param a: new port value
        """
        self.port = a

    def read_dhcp_msgs(self, d: bytes, parsed_msgs: list):
        """
        Recursively parse received bytes and return set of bytes to get list of DHCP messages
        :param d: bytes received from remote server
        :param parsed_msgs:
        :return: list of DHCP6 class instances that will represent all received messages (use .show() to print those)
        """
        if len(d) == 0:
            return parsed_msgs
        stop = int.from_bytes(d[:2], "big")
        if ipaddress.ip_address(self.addr) == 6:
            pkt = dhcp6.DHCP6(d[2:stop+2])
        else:
            pkt = dhcp.BOOTP(d[2:stop+2])
        pkt.build()
        parsed_msgs.append(pkt)
        if len(d[stop:]) > 0:
            parsed_msgs = self.read_dhcp_msgs(d[stop+2:], parsed_msgs)
        return parsed_msgs

    def connect_and_send(self, message: bytes = b"", how_many_connections: int = 1, parse: bool = True):
        """
        Open multiple connections to remote server, send message through each connection, save received data.
        Received data by default will be parsed as set of DHCP6 messages.
        :param message: bytes of DHPC6 bulklease query message, transaction id will be replaced, and different for
                        each connection. Also length of message will be added at the beginning to form proper TCP
                        bulk lease query message.
        :param how_many_connections: how many connection client should open, and send the same message (with changed
                                     transaction id) to each connection.
        :param parse: if True messages will be parsed using scapy libs, if False string of bytes will be returned
        :return: list of DHCP6 class instances or string of bytes
        """
        received = b""
        n = 1
        end = time.time() + 5
        socket_list = [socket.socket(socket.AF_INET6 if ipaddress.ip_address(self.addr).version == 6 else socket.AF_INET,
                                     socket.SOCK_STREAM) for _ in range(how_many_connections)]

        # this script do not change transaction ID of each message that we are sending
        for each_socket in socket_list:
            print(f"number: {n}: ", each_socket, self.addr)
            each_socket.connect((self.addr, int(self.port)))
            d = message[:1] + n.to_bytes(3, 'big') + message[4:]
            l = len(d)
            c_msg = l.to_bytes(2, 'big') + d
            # print(c_msg)
            each_socket.send(c_msg)
            n += 1

        while 1:
            read_sockets, write_sockets, error_sockets = select.select(socket_list, [], [], 3)
            for r_sock in read_sockets:
                data = r_sock.recv(4096)
                if not data:
                    pass
                else:
                    print(".", end="")
                    received += data
            if time.time() > end:
                break

        print("\nTCP bytes received: ", len(received))
        if parse:
            return self.read_dhcp_msgs(received, [])
        return received


def arg_pars():
    main_parser = argparse.ArgumentParser()
    main_parser.add_argument('--address', default="2001:db8:1::1000", help='IP address')
    main_parser.add_argument('--port', default=9000, help='port number')
    main_parser.add_argument('--connections', default=1, help='how many connections to open')
    main_parser.add_argument('--msg', default=None, help='msg in bytes (not tested)')
    main_parser.add_argument('--v4', default=False, action='store_true', help='send generated v4 message, by default'
                                                                              'it will be v6')

    my_args, _ = main_parser.parse_known_args()
    return my_args


def print_and_convert(msg):
    print(">>>>> Message that will be send:")
    if not isinstance(msg, bytes):
        msg.show()
        return raw(msg)
    return msg


def opt_82(subopt, value):
    if subopt not in [2, 12]:
        print("Suboption can be 2 - remote-id; 12 - relay-id. Exiting")
        exit()
    value = codecs.decode(value.replace(":", ""), 'hex')
    code = subopt.to_bytes(1, 'big')
    length = len(value)
    return code + length.to_bytes(1, 'big') + value


if __name__ == "__main__":
    args = arg_pars()
    # Available values of querytype in DHCP6OptLqQuery
    # 1: "query_by_address" - not blq
    # 2: "query_by_clientid" - not blq
    # 3: "query_by_relay_identifier"  - add dhcp6.DHCP6OptRelayId(duid=) to queryopts linkaddr can be 0::0 or specific
    # 4: "query_by_link_address" -  linkaddr has to be specific, no other options needed
    # 5: "query_by_remote_id" - add dhcp6.DHCP6OptRemoteID(remoteid=) to queryopts linkaddr can be 0::0 or specific
    msg = None

    if args.v4:
        # build v4 message https://www.rfc-editor.org/rfc/rfc6926.html#section-7.2
        # query by remote-id: The query carries a RAI (dhcp-agent-options (82) option) with a remote-id (2) sub-option.
        my_options = [("relay_agent_information", opt_82(2, "01:02:0c:03:0a:00"))]
        # query by relay-id: The query carries a RAI (dhcp-agent-options (82) option) with a relay-id (12) sub-option.
        my_options = [("relay_agent_information", opt_82(12, "01:02:0c:33:0a:11"))]
        msg = dhcp.BOOTP(xid=5)
        msg /= dhcp.DHCP(options=[("message-type", "bulk_leasequery")] + my_options + ["end"])

    else:
        # build v6 message
        duid = dhcp6.DUID_LLT(timeval=int(time.time()), lladdr="01:02:03:04:05:06")
        msg = dhcp6.DHCP6_Leasequery()
        msg /= dhcp6.DHCP6OptLqQuery(querytype=4, linkaddr="2001:db8:1::10",
                                     queryopts=dhcp6.DHCP6OptRemoteID(remoteid=b"\n\x00'\x00\x00\x01"))
        msg /= dhcp6.DHCP6OptClientId(duid=duid)

    msg = print_and_convert(msg)
    # send message
    c = TcpClient(args.address, args.port)
    msgs = c.connect_and_send(msg if args.msg is None else args.msg, how_many_connections=args.connections)
    print(">>>>> Messages received:")
    for x in msgs:
        x.show()
