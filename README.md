# tcp-python-client-blq



Sent BLQ message over TCP, using multiple connections, parse and print responses

**INSTALL**
```
git clone https://gitlab.isc.org/wlodek/tcp-python-client-blq.git
cd tcp-python-client-blq
python3 -m venv venv-blq
source venv-blq/bin/activate
pip install --upgrade pip
pip3 install -r requirements.txt
```
**USAGE**

tcp-blq.py can be imported as module or used directly. Modify lines 99-101 to build your own BLQ query

```
./tcp-bql.py
```
